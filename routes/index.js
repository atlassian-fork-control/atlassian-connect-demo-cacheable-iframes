module.exports = function (app, addon, jwt) {

  // Disable sending ETag response headers so the browser doesn't attempt to validate the
  // cached page it may have.
  app.disable('etag');
  app.set('etag', false);

  // healthcheck
  app.get('/healthcheck', function (req, res) {
    res.sendStatus(200);
  });

  // Root route. This route will serve the `atlassian-connect.json` unless the
  // documentation url inside `atlassian-connect.json` is set
  app.get('/', function (req, res) {
    res.format({
      // If the request content-type is text-html, it will decide which to serve up
      'text/html': function () {
        res.redirect('/atlassian-connect.json');
      },
      // This logic is here to make sure that the `atlassian-connect.json` is always
      // served up when requested by the host
      'application/json': function () {
        res.redirect('/atlassian-connect.json');
      }
    });
  });

  // Receive a context token for for secure transfer of context from the app client
  // to the app server. Note the argument to the addon.authenticate() method indicates
  // to skip QSH verification since context tokens don't have QSH claims.
  app.get('/handle-context-token', function (req, res) {
    var token = req.query['context-token'];
    if (token) {
      try {
        var unverifiedClaims = jwt.decode(token, '', true); // decode without verification;
        var issuer = unverifiedClaims.iss;
        if (issuer) {
          var clientKey = issuer;
          addon.settings.get('clientInfo', clientKey).then(function (settings) {
            var secret = settings.sharedSecret;
            var verifiedClaims = jwt.decode(token, secret, false);
            var context = verifiedClaims.context;
            var response = {
              status: 'ok',
              context: context
            };
            var responseJson = JSON.stringify(response, null, 3);
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
          }).catch(function(error) {
            res.status(400).send({
              status: 'error',
              message: 'Unable to verify JWT token: ' + error
            });
          });
        }
      } catch (error) {
        res.status(400).send({
          status: 'error',
          message: 'Unable to decode JWT token: ' + error
        });
      }
    }
  });

  // Jira app descriptor
  app.get('/jira-app-descriptor', function (req, res) {
    var descriptor = require('../jira-app-descriptor.json');
    var descriptorJson = JSON.stringify(descriptor, null, 3);
    descriptorJson = descriptorJson.split('{{localBaseUrl}}').join(addon.config.localBaseUrl());
    descriptorJson = descriptorJson.split('{{environment}}').join(addon.config.environment());
    res.setHeader('Content-Type', 'application/json');
    res.send(descriptorJson);
  });

  app.get('/cacheable-iframes-guide', function (req, res) {
      res.setHeader('Cache-Control', 'private, max-age=3600, s-maxage=3600');
      var serverRenderTime = new Date().getTime();
      res.render('cacheable-iframes-guide', {
        serverRenderTimestamp: serverRenderTime
      });
    }
  );

  // cacheable iframes guide for macros does not require authentication
  app.get('/cacheable-iframes-guide-macro', function (req, res) {
        var serverRenderTime = new Date().getTime();
        require('sleep').sleep(3);
        res.setHeader('Cache-Control', 'private, max-age=31557600, s-maxage=31557600');
        res.set('Expires', 'Mon, 27 Dec 2068 03:03:03 GMT');

        // Rendering a template is easy; the `render()` method takes two params: name of template
        // and a json object to pass the context in
        res.render('cacheable-iframes-guide', {
          serverRenderTimestamp: serverRenderTime
        });
      }
  );

  // cacheable iframes guide for macros does not require authentication
  app.get('/macro-editor', function (req, res) {
      var serverRenderTime = new Date().getTime();
      require('sleep').sleep(3);
      res.setHeader('Cache-Control', 'private, max-age=3600, s-maxage=3600');

      // Rendering a template is easy; the `render()` method takes two params: name of template
      // and a json object to pass the context in
      res.render('macro-editor', {
        serverRenderTimestamp: serverRenderTime
      });
    }
  );

  // cacheable iframes guide for macros does not require authentication
  app.get('/macro-view', function (req, res) {
      var serverRenderTime = new Date().getTime();
      require('sleep').sleep(3);
      res.setHeader('Cache-Control', 'private, max-age=3600, s-maxage=3600');

      // Rendering a template is easy; the `render()` method takes two params: name of template
      // and a json object to pass the context in
      res.render('macro-view', {
        serverRenderTimestamp: serverRenderTime
      });
    }
  );

  app.get('/generic-dialog', function (req, res) {
    var serverRenderTime = new Date().getTime();
    require('sleep').sleep(3);
    res.setHeader('Cache-Control', 'private, max-age=31557600, s-maxage=31557600');
    res.set('Expires', 'Mon, 27 Dec 2068 03:03:03 GMT');

    res.render('generic-dialog', {
      pageId: req.query['page.id'],
      title: 'Generic Dialog',
      serverRenderTimestamp: serverRenderTime
    });
  });

  // load any additional files you have in routes and apply those to the app
  {
    var fs = require('fs');
    var path = require('path');
    var files = fs.readdirSync("routes");
    for (var index in files) {
      var file = files[index];
      if (file === "index.js") continue;
      // skip non-javascript files
      if (path.extname(file) != ".js") continue;

      var routes = require("./" + path.basename(file));

      if (typeof routes === "function") {
        routes(app, addon);
      }
    }
  }
};
